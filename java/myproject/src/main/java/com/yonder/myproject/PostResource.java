package com.yonder.myproject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;

import com.yonder.myproject.model.Post;

import repository.PostRepository;

/**
 * Root resource (exposed at "hello" path)
 */
@Path("posts")
public class PostResource {
	private PostRepository repo = new PostRepository();
    
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public List<Post> getAll() {
		return repo.getAll();   
	}
	    

    @POST
    @Consumes("application/x-www-form-urlencoded;charset=UTF-8")
    @Produces(MediaType.APPLICATION_XML)
    public String postIt(
            @FormParam("id") String id,
            @FormParam("description") String description,
            @FormParam("author") String author)  {
        //return "Hello !";
    	//posturi.add(new Post(id,descriere,author));
    	repo.addPost(new Post(id,description,author));
    	return "POST reusit";
    }
}

