package com.yonder.myproject.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Post implements Serializable {
	private String id;
	private String description;
	private String author;
	
	public Post(String id, String description, String autor){
		this.id = id;
		this.description = description;
		this.author = autor;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public String getAuthor() {
		return this.author;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setDescription(String desc) {
		this.description = desc;
	}
	
	public void setAuthor(String autor) {
		this.author = autor;
	}
	
	public String toString() {
		return "id: " + id + " description: " + description + " author: " + author;
	}
	
}
