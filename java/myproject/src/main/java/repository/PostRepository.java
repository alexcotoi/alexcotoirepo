package repository;

import java.util.ArrayList;
import java.util.List;

import com.yonder.myproject.model.Post;

public class PostRepository {
	private List<Post> posturi = new ArrayList();
	
	public PostRepository() {
		posturi.add(new Post("o","o","o"));
	}
	
	public List<Post> getAll(){
		return posturi;
	}
	
	public void addPost(Post p) {
		posturi.add(p);
	}
}
