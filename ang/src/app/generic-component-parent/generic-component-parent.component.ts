import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';

@Component({
  selector: 'app-generic-component-parent',
  templateUrl: './generic-component-parent.component.html',
  styleUrls: ['./generic-component-parent.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GenericComponentParentComponent implements OnInit {

    @Input() dataFromParent : string;
    
  constructor() { }

  ngOnInit() {
      console.log(this.dataFromParent);
  }

}
