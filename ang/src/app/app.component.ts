import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  //title = 'app';
  //title = 'Welcome to Alex website';
  //description = 'Descrierea lui Alex.';
  private _description: string = 'Descrierea lui Alex !';
  dataForChild: string = 'De trimis la copil !!';
}
